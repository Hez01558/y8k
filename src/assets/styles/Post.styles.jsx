import styled from "styled-components";

const PostContainer = styled.div`
  position: absolute;
  top: 10vh;
  left: 50%;
  transform: translate(-50%, 0);
  width: 70vw;

  font-family: IBM3270;

  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: center;
`;

const PostTitle = styled.div`
  margin: 1vw;
  width: 100%;

  font-size: 3vw;
  color: white;
`;

const PostRepliesContainer = styled.div`
  position: relative;
  width: 100%;

  border: solid red 1px;

  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: center;
`;

const PostReplyContainer = styled.div`
  position: relative;
  width: 100%;

  border-bottom: solid red 1px;

  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: center;
`;

const PostReplyHeaderContainer = styled.div`
  position: relative;
  height: 1.5vw;
  width: 100%;

  border-bottom: dashed red 1px;

  display: flex;
  justify-content: space-between;
  align-items: center;

  font-size: 0.8vw;

  cursor: pointer;

  color: white;
  &:hover {
    color: red;
    text-decoration: underline;
  }
`;

const PostReplyHeaderAuthor = styled.div`
  margin: 1vw;

  color: inherit;
  text-decoration: inherit;

  font-size: inherit;
`;

const PostReplyHeaderTime = styled.div`
  margin: 1vw;

  color: white;
  text-decoration: none;

  font-size: inherit;
`;

const PostReplyContent = styled.div`
  padding: 1vw;
  min-height: 2vw;
  width: 90%;

  color: white;

  a {
    color: red !important;
  }

  blockquote {
    padding: 0.01% 1%;
    border-left: solid 1px red;
  }
`;

const PostBackContainer = styled.div`
  padding: 1%;
  width: 95%;
`;

const PostBackLink = styled.a`
  color: red;
`;

export {
  PostContainer,
  PostTitle,
  PostRepliesContainer,
  PostReplyContainer,
  PostReplyHeaderContainer,
  PostReplyHeaderAuthor,
  PostReplyHeaderTime,
  PostReplyContent,
  PostBackContainer,
  PostBackLink,
};
