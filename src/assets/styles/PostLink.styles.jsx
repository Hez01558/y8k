import styled from "styled-components";

const PostLinkContainer = styled.div`
  position: relative;
  height: 15vh;
  width: 100%;

  border-bottom: solid red 1px;
  cursor: pointer;

  &:hover {
    text-decoration: underline;
    color: red;
  }
`;

const PostLinkTitle = styled.div`
  position: absolute;
  top: 10%;
  left: 2%;

  font-size: 2vw;
  text-decoration: inherit;
  color: inherit;
`;

const PostLinkAuthor = styled.div`
  position: absolute;
  bottom: 10%;
  left: 2%;

  font-size: 1vw;
  text-decoration: inherit;
  color: inherit;
`;

const PostLinkTime = styled.div`
  position: absolute;
  bottom: 10%;
  right: 2%;

  font-size: 1vw;
  text-decoration: inherit;
  color: inherit;
`;

export { PostLinkContainer, PostLinkTitle, PostLinkAuthor, PostLinkTime };
